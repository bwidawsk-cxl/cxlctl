# Utility to bring forth the power of cxl_rs

## Install

The easiest way to do this is to create a Rust workspace and have cxlctl and
cxl_rs and projects within the workspace.
```
mkdir cxl-rs
cd cxl-rs
cat << EOF > Cargo.toml
[workspace]
members = [
	"cxlctl",
	"cxl_rs",
	]
EOF
git clone https://gitlab.com/bwidawsk-cxl/cxlctl.git
git clone https://gitlab.com/bwidawsk-cxl/cxl_rs.git
cargo test
```
## Running

On a system with CXL devices, the following commands should work:
```
cxl info mem0 # Obtain information about the 0th memory edvice
cxl list # List all CXL devices in the system
cxl send identify mem0 # Send the identify command to the 0th memory device.

# Example
./cxlctl  send identify mem0
{
  "fw_revision": "BWFW VERSION 00",
  "total_capacity": 268435456,
  "volatile_capacity": 0,
  "persistent_capacity": 268435456,
  "partition_alignment": 0,
  "info_event_log_size": 0,
  "warn_event_log_size": 0,
  "fail_event_log_size": 0,
  "fatal_event_log_size": 0,
  "lsa_size": 1024,
  "poison_list_max_records": [
    0,
    0,
    0
  ],
  "inject_poison_limit": 0,
  "poison_caps": 0,
  "telemetry": 0
}
```

## QEMU emulation

The easiest way to run as of now, with no real CXL 2.0 devices in existence is
the run_qemu script [found here](https://github.com/pmem/run_qemu/). See that
README for how to start a CXL capable system.
