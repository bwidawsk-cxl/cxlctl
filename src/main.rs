use clap::{crate_authors, crate_version, value_t, App, AppSettings, Arg, ArgMatches, SubCommand};
use cxl_rs::{lsa::CommonLabelProperties, Memdev};
use std::{
    fs,
    fs::File,
    io::{Error, ErrorKind, Read, Write},
    path::Path,
    str,
    sync::atomic::{AtomicUsize, Ordering},
};
use uuid::Uuid;

static VERBOSITY: AtomicUsize = AtomicUsize::new(0);

/// Instantiate a memory device based on the default sysfs location.
///
/// # Arguments
///
/// * `dev` - A name of a device, ie. mem0
///
/// # Examples
///
/// ```
/// let memdev = memdev!("mem");
/// let cmds = memdev.get_commands();
/// ```
macro_rules! memdev {
    ( $dev:expr ) => {{
        let d = format!("/sys/bus/cxl/devices/{}", $dev);
        Memdev::new(d.as_str()).unwrap()
    }};
}

macro_rules! cxl_info {
    ($($tts:tt)*) => {
        if VERBOSITY.load(Ordering::Relaxed) > 0 {
            println!("{}", format!($($tts)*));
        }
    };
}

/// Returns list of memory devices
fn get_device_list() -> Result<Vec<Memdev>, std::io::Error> {
    let files = cxl_rs::get_sysfs_devices(None)?;
    let mut devs = Vec::<Memdev>::new();

    for val in files.iter() {
        if let Some(memdev) = Memdev::new(val.1.to_str().unwrap()) {
            devs.push(memdev);
        }
    }
    Ok(devs)
}

fn cmd_enabled(memdev: &Memdev, cmd: ::std::os::raw::c_uint) -> Result<bool, std::io::Error> {
    let enabled: bool = memdev.is_command_enabled(cmd)?;

    match enabled {
        true => Ok(true),
        false => Err(Error::new(
            ErrorKind::Other,
            format!("Command {} is disabled on {}", cmd, memdev),
        )),
    }
}

fn slots(v: String) -> Result<(), String> {
    match v.parse::<u32>() {
        Ok(slots) => {
            if slots > 1472 {
                return Err(String::from("Max number of slots is 1472"));
            }
            if slots < 3 {
                return Err(String::from("Minimum number of slots is 3"));
            }
        }
        Err(e) => return Err(e.to_string()),
    }

    Ok(())
}

fn create_region(region: &ArgMatches<'_>) -> Result<(), std::io::Error> {
    let size = value_t!(region, "size", usize).unwrap_or_else(|e| e.exit());
    let ways = value_t!(region, "interleave", usize).unwrap_or_else(|e| e.exit());
    let region_uuid = Uuid::new_v4();
    for i in 0..ways {
        let file = region.value_of("file").unwrap();
        let mut file = File::create(format!("{}{}.bin", file, i))?;
        let region = cxl_rs::lsa::region_label::RegionLabel::new()
            .size(size / ways)
            .uuid(region_uuid)
            .interleave((ways, i));
        file.write_all(region.as_bytes())?;
    }
    if region.is_present("namespace") {
        let mut file = File::create("namespace.bin")?;
        let namespace = cxl_rs::lsa::namespace_label::NamespaceLabel::new()
            .name("Naive Namespace")
            .size(size)
            .region_uuid(region_uuid);
        file.write_all(namespace.as_bytes())
    } else {
        Ok(())
    }
}

fn main() -> Result<(), std::io::Error> {
    let memdevs: Vec<Memdev> = get_device_list()?;
    let dev_names: Vec<&str> = memdevs.iter().map(|d| d.get_name()).collect();

    let matches = App::new("cxlctl")
        .author(crate_authors!())
        .about("CXL control utility")
        .arg(
            Arg::with_name("v")
                .short("v")
                .multiple(true)
                .help("Sets the level of verbosity"),
        )
        .setting(AppSettings::ArgRequiredElseHelp)
        .setting(AppSettings::InferSubcommands)
        .subcommand(
            SubCommand::with_name("info")
                .about("Lists device information")
                .arg(
                    Arg::with_name("dev")
                        .help("The device")
                        .required(true)
                        .possible_values(&dev_names)
                        .index(1),
                )
        )
        .subcommand(
            SubCommand::with_name("list")
                .about("Lists attributes for devices")
                .arg(
                    Arg::with_name("filter")
                        .help("Filter devices by name")
                        .possible_values(&dev_names)
                        .required(false),
                ),
        )
        .subcommand(
            SubCommand::with_name("create-region")
                .about("Create a new CXL region.")
                .args(&[
                    Arg::from_usage("-i, --interleave [WAYS] 'Number of interleave ways'")
                         .default_value("1"),
                    Arg::from_usage("-s, --size [SIZE] 'Size of the region'")
                         .required(true),
                    Arg::with_name("namespace")
                         .short("-n")
                         .required(false)
                         .takes_value(false)
                         .help("Create a namespace label for this region. It will be named namespace.bin."),
                    Arg::with_name("file")
                         .help("FILE to write the region out to.")
                         .default_value("region")
                ]),
        )
        .subcommand(
            SubCommand::with_name("lsa")
                .about("Label Storage Area operations")
                .subcommand(
                    SubCommand::with_name("create")
                        .about("Create a label storage area.")
                        .args(&[
                            Arg::from_usage("--simple 'Create the most basic LSA with some default region/namespace sizes'")
                                .overrides_with_all(&["slots", "labels"]),
                            Arg::from_usage("-s, --slots=[SLOTS] 'Number of slots to allocate for the LSA'")
                                .default_value("3")
                                .validator(slots),
                            Arg::with_name("labels")
                                .help("Labels to add to this LSA")
                                .multiple(true)
                                .long("labels")
                                .takes_value(true)
                                .short("l"),
                            Arg::with_name("file")
                                .help("FILE to write the LSA out to.")
                                .default_value("lsa.bin")
                                .required(false),
                        ]),
                )
                .subcommand(
                    SubCommand::with_name("linfo")
                        .about("Print information about the specified label")
                        .args(&[
                            Arg::with_name("file")
                                .help("FILE to write the LSA out to.")
                                .required(true),
                        ]),
                )
                .subcommand(
                    SubCommand::with_name("verify")
                        .about("Verify a label storage area")
                        .args(&[Arg::with_name("file")
                            .help("FILE to read the LSA out from.")
                            .required(true)
                            .last(true)]),
                ),
        )
        .subcommand(
            SubCommand::with_name("send")
                .about("Sends a command to the given device")
                .arg(
                    Arg::with_name("cmd")
                        .help("The CXL command to send")
                        .required(true)
                        .possible_values(&["identify"])
                        .index(1),
                )
                .arg(
                    Arg::with_name("dev")
                        .help("The memory device to send to")
                        .required(true)
                        .possible_values(&dev_names)
                        .index(2),
                ),
        )
        .version(crate_version!())
        .get_matches();

    VERBOSITY.store(matches.occurrences_of("v") as usize, Ordering::Relaxed);

    // All output is serialized (JSON) by default.
    // TODO: Support human readable output.
    let mut serialized: String = String::new();

    if let Some(region) = matches.subcommand_matches("create-region") {
        create_region(region)?;
    }

    if let Some(matches) = matches.subcommand_matches("info") {
        let memdev = memdev!(matches.value_of("dev").unwrap());
        let cmds = memdev.get_commands()?;
        serialized = serde_json::to_string_pretty(&cmds).unwrap();
    }

    if let Some(matches) = matches.subcommand_matches("list") {
        let mut devs = memdevs;
        if let Some(f) = matches.value_of("filter") {
            devs = devs
                .into_iter()
                .filter(|dev| dev.get_name().contains(f))
                .collect();
        }
        serialized = serde_json::to_string_pretty(&devs).unwrap();
    }

    if let Some(matches) = matches.subcommand_matches("lsa") {
        match matches.subcommand() {
            ("create", Some(create)) => {
                let file = create.value_of("file").unwrap();
                let mut file = File::create(file)?;
                let mut lsa = cxl_rs::lsa::LabelStorageArea::new(1472);
                let mut slots = value_t!(create, "slots", usize).unwrap_or_else(|e| e.exit());
                let mut used_slots: Vec<usize> = Vec::new();

                if create.is_present("simple") {
                    let size = 8 << 30;
                    let region = lsa.push_region_label(size)?;
                    let namespace = lsa.push_namespace_label(size)?;
                    let empty = lsa.push_empty_label()?;
                    lsa.retain_label_slots(0, vec![region, namespace, empty]);
                } else if create.is_present("labels") {
                    let labels: Vec<_> = create.values_of("labels").unwrap().collect();
                    for (i, label) in labels.iter().enumerate() {
                        let mut file = File::open(label)?;
                        let mut buffer = Vec::new();

                        file.read_to_end(&mut buffer)?;

                        let mut deserde_label = cxl_rs::lsa::label_from_bytes(buffer.as_slice())?;
                        cxl_info!("Found: {}", deserde_label);
                        deserde_label.set_slot(i);
                        used_slots.push(lsa.push_label(deserde_label)?);

                        slots -= 1;
                    }
                }

                for _i in 0..slots {
                    used_slots.push(lsa.push_empty_label()?);
                }

                lsa.retain_label_slots(0, used_slots);
                lsa.set_active(0);
                file.write_all(&lsa.to_vec()?.as_slice())?;
                cxl_info!("{}", serde_json::to_string_pretty(&lsa).unwrap());
            }
            ("linfo", Some(linfo)) => {
                let filename = linfo.value_of("file").unwrap();
                if !Path::new(filename).is_file() {
                    panic!("{} is not a file", filename);
                }
                let mut file = File::open(filename)?;
                let mut buffer = Vec::new();
                file.read_to_end(&mut buffer)?;
                // Try to load LSA first, then Label. If both fail, peace out
                serialized = match cxl_rs::lsa::LabelStorageArea::from_bytes(&buffer) {
                    Ok(lsa) => serde_json::to_string_pretty(&lsa).unwrap(),
                    Err(_e) => match cxl_rs::lsa::label_from_bytes(&buffer) {
                        Ok(label) => serde_json::to_string_pretty(&label).unwrap(),
                        Err(e) => panic!("{}", e.to_string()),
                    },
                };
            }
            ("verify", Some(verify)) => {
                let filename = verify.value_of("file").unwrap();
                let metadata = fs::metadata(filename)?;
                assert!(metadata.len() % 256 == 0);
                let mut file = File::open(filename)?;
                let mut buffer = Vec::new();
                file.read_to_end(&mut buffer)?;
                let lsa = cxl_rs::lsa::LabelStorageArea::from_bytes(&buffer)?;
                lsa.verify()?;
                cxl_info!("{}", serde_json::to_string_pretty(&lsa).unwrap());
            }
            _ => {}
        }
    }

    if let Some(matches) = matches.subcommand_matches("send") {
        let memdev = memdev!(matches.value_of("dev").unwrap());

        // Note, it's safe to call unwrap() because the arg is required
        match matches.value_of("cmd").unwrap() {
            "identify" => {
                cmd_enabled(
                    &memdev,
                    cxl_rs::send::send_bindings::CXL_MEM_COMMAND_ID_IDENTIFY,
                )?;
                let out = Some(memdev.identify()?);
                serialized = serde_json::to_string_pretty(&out).unwrap();
            }
            _ => unreachable!(),
        }
    }

    println!("{}", serialized);

    Ok(())
}
